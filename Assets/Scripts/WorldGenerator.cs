﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ClassicalSharp.Generator;

/*
Instead of having one game object per block:
    Create and render a block for each gameobject making contact with air
    Don't do anything for the rest


    When a block is added or removed, update accordingly (just in that spot)
*/


public class WorldGenerator : MonoBehaviour {
    public int width = 256;
    public int height = 64;
    public int waterLevel = 32;
    public GameObject dirtPrefab;
    public GameObject lavaPrefab;
    public GameObject stonePrefab;
    public GameObject playerPrefab;
    public GameObject grassPrefab;
    public GameObject soundPlayer;

    [HideInInspector]
    public float blockWidth;
    [HideInInspector]    
    public GameObject[,,] blockObjects;
    [HideInInspector]
    public BlockType[,,] blocks;

    [HideInInspector]
    public int[,] heightMap;
    
    [HideInInspector]
    public enum BlockType { Air, Lava, Stone, Dirt, Grass };

    // Map generation, based on this outline:
    // https://github.com/UnknownShadow200/ClassicalSharp/wiki/Minecraft-Classic-map-generation-algorithm
    void Start () {
        heightMap = new int[width, width];
        blocks = new BlockType[width, height, width];
        blockObjects = new GameObject[width, height, width];
        blockWidth = dirtPrefab.GetComponent<Renderer>().bounds.size.x;
        CreateHeightMap();
        CreateStrata();
        CreateWorld();
        PlacePlayer();
        CreateGrass();
        soundPlayer.SetActive(true);
    }

    //Create the grass layer on top
    void CreateGrass()
    {
        for (int x=0; x < width; x++)
        {
            for (int z=0; z < width; z++)
            {
                int height = heightMap[x, z];
                BlockType blockAbove = blocks[x, height+1, z];
                if (blockAbove == BlockType.Air)
                {
                    blocks[x, height, z] = BlockType.Grass;
                    Vector3 pos = new Vector3(x * blockWidth, height * blockWidth, z * blockWidth);
                    Destroy(blockObjects[x, height, z]);
                    blockObjects[x, height, z] = Instantiate(grassPrefab, pos, Quaternion.identity);
                    Block b = blockObjects[x, height, z].GetComponent<Block>();
                    b.xPos = x;
                    b.yPos = height;
                    b.zPos = z;
                    b.type = BlockType.Grass;
                }
                

            }
        }
    }
	
    //Find the middle of the map and place the player on the ground
    void PlacePlayer()
    {
        Vector3 playerPos = new Vector3(0, 0, 0);
        for (int y=0; y < height; y++)
        {
            if (blocks[width/2,y,width/2] == BlockType.Air)
            {
                playerPos = new Vector3(width / 2, y * blockWidth, width / 2);
                break;
            }
        }
        Instantiate(playerPrefab, playerPos, Quaternion.identity);
    }

//Instantiate blocks
    void CreateWorld()
    {
        for (int x=0; x < width; x++)
        {
            for (int y=0; y < height; y++)
            {
                for (int z=0; z < width; z++)
                {
                    //Don't instantiate blocks that won't be visible
                    //This improves performance drastically.
                    if (!surroundingIsAir(x,y,z) || blocks[x, y, z] == BlockType.Air)
                    {
                        blockObjects[x, y, z] = null;
                        continue;
                    }
                    GameObject toInstantiate = null;
                    if (blocks[x, y, z] == BlockType.Lava)
                    {
                        toInstantiate = lavaPrefab;
                    }
                    else if (blocks[x, y, z] == BlockType.Stone)
                    {
                        toInstantiate = stonePrefab;
                    }
                    else if (blocks[x,y,z] == BlockType.Dirt)
                    {                     
                        toInstantiate = dirtPrefab;
                    }

                    Vector3 pos = new Vector3(x * blockWidth, y * blockWidth, z * blockWidth);
                    blockObjects[x, y, z] = Instantiate(toInstantiate, pos, Quaternion.identity);
                    Block b = blockObjects[x, y, z].GetComponent<Block>();
                    b.xPos = x;
                    b.yPos = y;
                    b.zPos = z;
                    if (blocks[x,y,z] == BlockType.Lava)
                    {
                        b.type = BlockType.Lava;
                    } else if (blocks[x,y,z] == BlockType.Stone)
                    {
                        b.type = BlockType.Stone;
                    } else if (blocks[x,y,z] == BlockType.Dirt)
                    {
                        b.type = BlockType.Dirt;
                    }
                }
            }
        }
    }

    public void destroyBlock(int xPos, int yPos, int zPos)
    {
        //Debug.Log(string.Format("Destroying block[{0},{1},{2}] with Blocktype {3}", xPos, yPos, zPos, blocks[xPos, yPos, zPos]));
        blocks[xPos, yPos, zPos] = BlockType.Air;
        Destroy(blockObjects[xPos, yPos, zPos]);
        //Make surrounding blocks visible
        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                for (int z = -1; z <= 1; z++)
                {
                    //Debug.Log(string.Format("Checking block[{0},{1},{2}] with Blocktype {3}. isNull={4}", xPos + x, yPos + y, zPos + z, blocks[xPos + x, yPos + y, zPos + z], blockObjects[xPos + x, yPos + y, zPos + z] == null));
                    //If out of range, it's not a thing
                    if (x + xPos < 0 || x + xPos > width - 1 || y + yPos < 0 || y + yPos > height - 1 ||
                        z + zPos < 0 || z + zPos > width - 1)
                    {
                       // Debug.Log("Out of range, continuing.");
                        continue;
                    }
                    GameObject toInstantiate = null;
                   
                    //If you should put a lava block there and nothing is there already, we want to put a lava block
                    if (blocks[x + xPos, y + yPos, z + zPos] == BlockType.Lava && blockObjects[x+xPos,y+yPos,z+zPos] == null)
                    {
                        toInstantiate = lavaPrefab;
                    }
                    else if (blocks[x + xPos, y + yPos, z + zPos] == BlockType.Stone && blockObjects[x + xPos, y + yPos, z + zPos] == null)
                    {
                        toInstantiate = stonePrefab;
                    }
                    else if (blocks[x + xPos, y + yPos, z + zPos] == BlockType.Dirt && blockObjects[x + xPos, y + yPos, z + zPos] == null)
                    {
                        toInstantiate = dirtPrefab;
                    }
                    else
                    {
                        continue;
                    }
                    Vector3 pos = new Vector3((x+xPos) * blockWidth, (y+yPos) * blockWidth, (z+zPos) * blockWidth);
                    blockObjects[x + xPos, y + yPos, z + zPos] = Instantiate(toInstantiate, pos, Quaternion.identity);
                    Block b = blockObjects[x + xPos, y + yPos, z + zPos].GetComponent<Block>();
                    b.xPos = x + xPos;
                    b.yPos = y + yPos;
                    b.zPos = z + zPos; 
                    if (blocks[b.xPos,b.yPos,b.zPos] == BlockType.Lava)
                    {
                        b.type = BlockType.Lava;
                    } else if (blocks[b.xPos,b.yPos,b.zPos] == BlockType.Stone)
                    {
                        b.type = BlockType.Stone;
                    }
                    else if (blocks[b.xPos, b.yPos, b.zPos] == BlockType.Dirt)
                    {
                        b.type = BlockType.Dirt;
                    }
                }
            }
        }
    }

    //True iff one of the surrounding blocks is air
    bool surroundingIsAir(int xPos, int yPos, int zPos)
    {
        for (int x=-1; x <=1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                for (int z = -1; z <= 1; z++)
                {
                    //If out of range, it's not a thing
                    if (x + xPos < 0 || x + xPos > width - 1 || y + yPos < 0 || y + yPos > height-1 ||
                        z + zPos < 0 || z + zPos > width - 1)
                    {
                        continue;
                    }
                    if (blocks[x+xPos,y+yPos,z+zPos] == BlockType.Air)
                    {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    //Create strata separating lava, stone, and dirt
    void CreateStrata()
    {
        OctaveNoise noise = new ClassicalSharp.Generator.OctaveNoise(8, new ClassicalSharp.Generator.JavaRandom((new System.Random()).Next()));
        for (int x=0; x < width; x++)
        {
            for (int z = 0; z < width; z++)
            {
                float dirtThickness = (float)noise.Compute(x, z) / 24 - 4;
                float dirtTransition = heightMap[x, z];
                float stoneTransition = dirtTransition + dirtThickness;
                for (int y=0; y < height; y++)
                {
                    if (y == 0) blocks[x, y, z] = BlockType.Lava;
                    else if (y <= stoneTransition) blocks[x, y, z] = BlockType.Stone;
                    else if (y <= dirtTransition) blocks[x, y, z] = BlockType.Dirt;
                    else blocks[x, y, z] = BlockType.Air;
                }

            }
        }
        
    }

    void CreateHeightMap()
    {
        //Need 3 seperate noise functions, one for each noise generator
        JavaRandom rnd1 = new ClassicalSharp.Generator.JavaRandom((new System.Random()).Next());
        JavaRandom rnd2 = new ClassicalSharp.Generator.JavaRandom((new System.Random()).Next());
        CombinedNoise noise1 = new ClassicalSharp.Generator.CombinedNoise(new ClassicalSharp.Generator.OctaveNoise(8, rnd1), new OctaveNoise(8, rnd2));
        JavaRandom rnd3 = new ClassicalSharp.Generator.JavaRandom((new System.Random()).Next());
        JavaRandom rnd4 = new ClassicalSharp.Generator.JavaRandom((new System.Random()).Next());
        CombinedNoise noise2 = new ClassicalSharp.Generator.CombinedNoise(new ClassicalSharp.Generator.OctaveNoise(8, rnd3), new OctaveNoise(8, rnd4));
        JavaRandom rnd5 = new ClassicalSharp.Generator.JavaRandom((new System.Random()).Next());
        OctaveNoise noise3 = new ClassicalSharp.Generator.OctaveNoise(6, rnd5);

        //Set heightMap according to noise functions
        float heightResult = 0;
        for (int x = 0; x < width; x++)
        {
            for (int z = 0; z < width; z++)
            {
                float heightLow = (float)noise1.Compute(x * 1.3, z * 1.3) / 6 - 4;
                float heightHigh = (float)noise2.Compute(x * 1.3, z * 1.3) / 5 + 6;
                if (noise3.Compute(x, z) / 8 > 0)
                {
                    heightResult = heightLow;
                }
                else
                {
                    heightResult = (heightLow <= heightHigh) ? heightHigh : heightLow;
                }
                heightResult = heightResult / 2;
                if (heightResult < 0)
                {
                    heightResult *= 0.8f;
                }
                heightMap[x, z] = ((int)heightResult) + waterLevel;
                //Debug.Log("heightMap[" + x + "][" + z + "]=" + heightMap[x, z]);
            }
        }
    }

	// Update is called once per frame
	void Update () {
		
	}
}
