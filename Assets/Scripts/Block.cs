﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour {
    public int hitsToBreak;
    public int xPos, yPos, zPos;
    WorldGenerator wg;
    public WorldGenerator.BlockType type;
    public Material crackedMaterial;

	// Use this for initialization
	void Start () {
        wg = FindObjectOfType<WorldGenerator>();
	}
	
    public void Hit()
    {
        if (hitsToBreak == -1) return;
        hitsToBreak--;
        if (hitsToBreak == 0)
        {
            wg.destroyBlock(xPos, yPos, zPos);
        } else if (type == WorldGenerator.BlockType.Stone)
        {
            GetComponent<Renderer>().material = crackedMaterial;
        }
    }
}
